const express = require('express')
const cors = require('cors')
const fs = require('fs')
const path = require('path')
const morgan = require('morgan');
const app = express()
const port = 8080

app.use(express.urlencoded({ extended: true }));
app.use(express.json())
app.use(cors())

morgan.token('body', req => JSON.stringify(req.body))

app.use(morgan(':method :url :status :body'));

app.post('/api/files', (req, res) => {
	const filename = req.body.filename
	const content = req.body.content

	if(!content) {
		morgan.token('body', () => JSON.stringify({message: "Please specify 'content' parameter"}))
		return res.status(400).send({message: "Please specify 'content' parameter"}).end();
	}
	if(!filename) {
		morgan.token('body', () => JSON.stringify({message: "Please specify 'filename' parameter"}))
		return res.status(400).send({message: "Please specify 'filename' parameter"}).end();
	}
	
	const filePath = path.join(__dirname, 'public', filename)

	if(fs.existsSync(filePath) === true) {
		morgan.token('body', () => JSON.stringify({message: "File already exists"}))
		return res.status(400).send({message: "File already exists"}).end();
	}

	fs.mkdir('public', { recursive: true }, (err) => {
		if (err) {
			morgan.token('body', () => JSON.stringify({message: "Server error"}))
			return res.status(500).send({message: "Server error"}).end();
		}
	});
	fs.writeFile(filePath, content, (err) => {
		if(err) {
			morgan.token('body', () => JSON.stringify({message: "Server error"}))
			return res.status(500).send({message: "Server error"}).end();
		}
	})
	morgan.token('body', () => JSON.stringify({message: "File created successfully"}))
	res.status(200).send({message: "File created successfully"}).end();
})

app.get('/api/files', (req, res) => {
	const filePath = path.join(__dirname, 'public')

	fs.readdir(filePath, (err, files) => {
		if(err) {
			morgan.token('body', () => JSON.stringify({message: "Server error"}))
			return res.status(500).send({message: "Server error"}).end();
		}
		if(!files.length){
			morgan.token('body', () => JSON.stringify({message: "Server error"}))
			return res.status(400).send({message: "Client error"}).end();
		}
		morgan.token('body', () => JSON.stringify({message: "Success",files}))
		res.status(200).send({
			message: "Success",
			files
		}).end();
	});
})

app.get('/api/files/:filename', (req, res) => {
	const filename = req.params.filename
	const filePath = path.join(__dirname, 'public', filename)

	fs.readFile(filePath, 'utf-8', (err, content) => {
		if(err) {
			morgan.token('body', () => JSON.stringify({message: `No file with '${filename}' filename found`}))
			return res.status(400).send({message: `No file with '${filename}' filename found`}).end();
		}
		let uploadedDate = fs.statSync(filePath).birthtime
		morgan.token('body', () => JSON.stringify({message: "Success",filename,content,extension: path.extname(filePath).replace('.', ''),uploadedDate}))
		res.status(200).send({
			message: "Success",
			filename,
			content,
			extension: path.extname(filePath).replace('.', ''),
			uploadedDate
		}).end();
	})
})

app.delete('/api/files/delete/:filename', (req, res) => {
	const filename = req.params.filename
	const filePath = path.join(__dirname, 'public', filename)
	fs.unlink(filePath, (err) => {
		if(err) {
			morgan.token('body', () => JSON.stringify({message: `No file with '${filename}' filename found`}))
			return res.status(400).send({message: `No file with '${filename}' filename found`}).end();
		}
		morgan.token('body', () => JSON.stringify({message: "File deleted successfully"}))
		res.status(200).send({message: "File deleted successfully"}).end();
	})
})

app.put('/api/files/modify/:filename', (req, res, next) => {
	const filename = req.body.filename
	const content = req.body.content

	if(!content) {
		morgan.token('body', () => JSON.stringify({message: "Please specify 'content' parameter"}))
		return res.status(400).send({message: "Please specify 'content' parameter"}).end();
	}
	if(!filename) {
		morgan.token('body', () => JSON.stringify({message: "Please specify 'filename' parameter"}))
		return res.status(400).send({message: "Please specify 'filename' parameter"}).end();
	}
	const filePath = path.join(__dirname, 'public', filename)
	fs.readFile(filePath, 'utf-8', (err, data) =>{
		if(err) {
			morgan.token('body', () => JSON.stringify({message: "No file with '${filename}' filename found"}))
			return res.status(400).send({message: `No file with '${filename}' filename found`}).end();
		}
		const newValue = data.replace(/^\./gim, content);

		fs.writeFile(filePath, newValue, 'utf-8', (err) => {
			if(err) {
				morgan.token('body', () => JSON.stringify({message: "Server error"}))
				return res.status(500).send({message: "Server error"})
			}
			morgan.token('body', () => JSON.stringify({message: "File modified successfully"}))
			res.status(200).send({message: "File modified successfully"}).end();
		});
	});
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})